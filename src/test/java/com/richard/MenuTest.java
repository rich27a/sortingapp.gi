package com.richard;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.*;
@RunWith(Parameterized.class)
public class MenuTest {
    Menu menu = new Menu();

    @Parameterized.Parameter
    public int contador;

    @org.junit.Test(expected = IllegalArgumentException.class)
    public void testInvalidNumberOfElements() {
       menu.checkNumberOfArgs(contador);
    }

    @Parameterized.Parameters
    public static Collection testCases() {
        return Arrays.asList(new Object[][] { { 0},
                { 11 },
                { 14 },
                { 20 }, });
    }

}