package com.richard;

import org.junit.Assert;
import org.junit.Assume;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class SortingTest {

    enum Type {ONE, TEN}
    private int[] numbers;
    private int[] expected;
    private Type type;
    Sorting sorting = new Sorting();


    public SortingTest(Type type, int[] numbers, int[] expected){
        this.type = type;
        this.numbers = numbers;
        this.expected = expected;

    }

    @Test
    public void testOneElementCase() {
        Assume.assumeTrue(type == Type.ONE);
        sorting.sort(numbers);
        Assert.assertTrue(Arrays.equals(numbers, expected));
    }
    @Test
    public void testTenElementCase() {
        Assume.assumeTrue(type == Type.TEN);
        sorting.sort(numbers);
        Assert.assertTrue(Arrays.equals(numbers, expected));
    }

    @Parameterized.Parameters
    public static Iterable<Object[]> data(){
        return Arrays.asList(new Object[][] {
                {Type.ONE, new int[]{1}, new int[]{1}},
                {Type.ONE, new int[]{2}, new int[]{2}},
                {Type.ONE, new int[]{3}, new int[]{3}},
                {Type.ONE, new int[]{4}, new int[]{4}},
                {Type.TEN, new int[]{2,1,3,4,7,5,6,8,9,10}, new int[]{1,2,3,4,5,6,7,8,9,10}},
                {Type.TEN, new int[]{5,6,8,9,10,2,1,3,4,7}, new int[]{1,2,3,4,5,6,7,8,9,10}},
                {Type.TEN, new int[]{2,1,3,6,8,9,10,4,7,5}, new int[]{1,2,3,4,5,6,7,8,9,10}},
                {Type.TEN, new int[]{7,5,6,8,9,10,2,1,3,4}, new int[]{1,2,3,4,5,6,7,8,9,10}},

        });
    }
}