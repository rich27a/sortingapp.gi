package com.richard;

import java.util.*;

/**
 * Hello world!
 *
 */
public class App

{
    public static void main( String[] args ) {
        Menu menu = new Menu();
        Sorting sorting = new Sorting();

        menu.checkNumberOfArgs(args.length);
        int[] numeros = new int[args.length];
        for (int i = 0; i < numeros.length; i++) {
            numeros[i] = Integer.parseInt(args[i]);
        }

        sorting.sort(numeros);
    }
}
