package com.richard;

import java.util.Arrays;

public class Sorting {
    public void sort(int[] numeros){
        Arrays.sort(numeros);
        System.out.println("Numeros ordenados \n");
        for(Integer i: numeros){
            System.out.print(i +" ");
        }
    }
}
