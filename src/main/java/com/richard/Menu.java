package com.richard;

import java.util.List;
import java.util.Scanner;

public class Menu {
    private static final int MAX_ELEMENTS = 10;
    private static final int MIN_ELEMENTS = 10;


    public Menu(){

    }

    public void checkNumberOfArgs(int numArgs){
        if(numArgs < 1 || numArgs > 10)
            throw new IllegalArgumentException();
    }

}
